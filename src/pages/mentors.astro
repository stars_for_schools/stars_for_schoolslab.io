---
import { getCollection } from "astro:content";
import { Picture } from "@astrojs/image/components";
import Layout from "@layouts/Layout.astro";
import Container from "@components/container.astro";
import Sectionhead from "@components/sectionhead.astro";

// Filter team entries with 'draft: false' & date before current date
const publishedTeamMembers = await getCollection("team", ({ data }) => {
  return !data.draft && data.publishDate < new Date();
});
---

<Layout title="Mentors">
  <Container>
    <Sectionhead>
      <Fragment slot="title">Mentors</Fragment>
      <Fragment slot="desc">Stars for Schools relies on the generous support of physics experts and professionals to act as mentors for the programme and provide support for schools.</Fragment>
    </Sectionhead>

      <div class="flex flex-col gap-3 mx-auto max-w-4xl mt-16">
          <h2 class="font-bold text-3xl text-gray-800">
              Mentors / Ambassadors
          </h2>
          <p style="text-align: justify;" class="text-lg leading-relaxed text-slate-500">
              The <i>Stars for Schools</i> programme's mentors provide the key connection between the programme's management and material, and participating schools. Mentors, also called ambassadors by some, bring university-level physics experience directly to a school's students and, in many cases, they are professional stellar astrophysicsists.
          </p>
      </div>
      <div class="flex flex-col gap-3 mx-auto max-w-4xl mt-16">
          <h2 class="font-bold text-3xl text-gray-800">
              Mentor role
          </h2>
          <p style="text-align: justify;" class="text-lg leading-relaxed text-slate-500">
              Mentors work for their responsible employer or manager, usually a university. They should attend relevant outreach, and health and safety training provided by their university, particularly making sure they follow ethics requirements, and also respect any ethics or security requests of their school.
              Mentors visit schools at least once at the beginning of the programme and once at the end, and provide support either virtually or with in-person visits in between, depending on their school&apos;s requirements, and available time and funding.
              Through their regular, usually virtual, meetings with the programme director, mentors are key to providing feedback and updates about the programme so we can assess and improve it for the future.
              Mentors are also responsible for ensuring that participants, and their parents, sign the programme consent forms and are provided with the programme information document. 
          </p>
      </div>
      <div class="flex flex-col gap-3 mx-auto max-w-4xl mt-16">
          <h2 class="font-bold text-3xl text-gray-800">
              Communication with schools
          </h2>
          <p style="text-align: justify;" class="text-lg leading-relaxed text-slate-500">
              Mentors provide the primary means of communication between a school and the programme. At the start of the programme they should ensure the school provides the relevant information to teachers, students and parents, and that signed forms are sent to the programme leader, such as participant information sheets, consent forms, and the Raspberry Pi loan agreement. They should also ensure the students complete the provided questionnaires at the appropriate time, and communicate these results to the programme leader.
          </p>
      </div>
      <div class="flex flex-col gap-3 mx-auto max-w-4xl mt-16">
          <h2 class="font-bold text-3xl text-gray-800">
              <h2 class="font-bold text-3xl text-gray-800">
                  Data and security
              </h2>
          <p style="text-align: justify;" class="text-lg leading-relaxed text-slate-500">
              Mentors are required to keep data secure, particularly personal data such as names of participants. Participants are only referred to in almost all programme documents by their unique identification number. A <a href="https://en.wikipedia.org/wiki/Pretty_Good_Privacy">PGP</a>-encrypted spreadsheet to convert to and from the participant's name and ID number is then only available to the mentor, programme leader and their head of department (as a backup). In this way we maintain anonymized data to monitor the programme, and students' progress through it, at minimal risk to its participants even if the files are stolen (PGP encryption is considered next-to unbreakable).
              <br/>
              We ask that mentors have a recent <a href="https://www.gov.uk/request-copy-criminal-record">DBS check</a>. The expense for this may be claimed through supporting grants, please contact the programme director for more information.
          </p>
          </div>
          <div class="flex flex-col gap-3 mx-auto max-w-4xl mt-16">
              <h2 class="font-bold text-3xl text-gray-800">
                  Raspberry Pi hardware
              </h2>
              <p style="text-align: justify;" class="text-lg leading-relaxed text-slate-500">
                  Mentors need to be able to install and solve problems with the Raspberry Pi hardware. In many regards this is easy: just plug in the cables in the right places. If you've never installed an operating system for a Raspberry Pi before, have a look at <a href="https://www.youtube.com/watch?v=BpJCAafw2qE">this video</a> and <a href="https://www.youtube.com/watch?v=jRKgEXiMtns">this video</a>.

                  Troubleshooting <i>Window to the Stars</i> and, especially, <i>TWIN</i> is more difficult because this is a numerical code that can go wrong in many ways given the large number of options available to the user.
              </p>
          </div>
          <div class="flex flex-col gap-3 mx-auto max-w-4xl mt-16">
              <h2 class="font-bold text-3xl text-gray-800">
                  Raspberry Pi software
              </h2>
              <p style="text-align: justify;" class="text-lg leading-relaxed text-slate-500">
                  Troubleshooting <i>Window to the Stars</i> and, especially, <i>TWIN</i> requires some expertise because <i>WTTS</i> is a numerical code that can go wrong in many ways given the large number of options available to the user. The best thing to do is practice before meeting your students, go through the papers, documentation including the lab examples and summer schools on the <a href="https://r-izzard.surrey.ac.uk/window.html#doc">WTTS website</a>, try the examples, and, as always, ask if you get stuck so I can update this documentation.
              </p>
          </div>

          <a name="mentors">
              <div class="flex flex-col gap-3 mx-auto max-w-4xl mt-16">
                  <h2 class="font-bold text-3xl text-gray-800">
                      Mentor checklist
                  </h2>
                  <p style="text-align: justify;" class="text-lg leading-relaxed text-slate-500">
                      Mentors should make sure they do the following prior to embarking on the programme.
                      <ul class='List' style="list-style-type: circle;">
                          <li>Download and install the <a href="computing/#WTTS">WTTS software</a>, and try to run a few stellar tracks.</li>
                          <li>Read the <i>Stars for Schools</i> <a href="guide/">programme guide</a>, and be aware of the various tasks that students should attempt. Try them!</li>
                          <li>Revise knowledge of stellar evolution in advance of students asking questions.</li>
                          <li>Learn how to install WTTS on a Raspberry Pi (see <a href="computing/#raspberrypi">here</a>).</li>
                          <li>Meet, possibly virtually, with the programme leader. Discuss requirements and possible payment of expenses from the various grants.</li>
                          <li>Obtain required Raspberry Pis from the programme leader. This may require them to be posted.</li>
                          <li>Obtain a <a href="https://www.gov.uk/request-copy-criminal-record">DBS certificate</a>.</li>
                          <li>Read the various forms, e.g. participant information, consent, Raspberry Pi loan agreement, recruiment document, which are found on <a href="/forms/">this page</a>.</li>
                          <li>Ensure their school has copies of the forms described above, and that signed copies are sent to the programme leader.</li>
                          <li>Ensure their school provides gatekeeper permission, in email or written form, to the programme leader. These are required <i>before</i> the mentor starts the programme at their school.</li>
                          <li>Get in touch with their school to set up the first meeting and check if the school has any special requirements, e.g. with regard to security.</li>
                          <li>Ensure questionnaires are filled in when required, e.g. at the start and end of the programme.</li>
                          <li>Book travel, e.g. train tickets, in the manner required by their university or employer (who may required booking through a travel company). If you are at Surrey please use <a href="https://claritybusinesstravel.com/why-us/data-and-insight/go2book/">Go2Book</a>.  You should <i>not</i> book directly through Clarity: they charge £10 per booking, which is crazy for a local journey. Booking through Go2Book costs us only 50p. You should never trust a company with a number in its name, but...</li>
                      </ul>
                      
                          All that remains is to go meet the students and teachers, and have a great time helping them evolve stars!
                  Further meetings should be organised as required, preferably at least every few weeks, be these in person or virtual.</li>
                      </ul>
                  </p>
              </div>
          </a>

  </Container>
</Layout>

